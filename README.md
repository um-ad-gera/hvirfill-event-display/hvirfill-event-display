# Hvirfill Event Display
This pluging fetches information from hvirfill.reykjavik.is, based on given
parameters (configurable) and displays them, as a single item or lists of 
items.

## Search parameters
To configure the search parameters, please refer to https://hvirfill.reykjavik.is/find

For convenience, the query is split up into two parts: query itself and search parameters.
It gives better readability of the query:
query: find?sort=start&format=json
search parameters: &all=Bókasafn Hafnarfjarðar

The search parameters can be changed within the shortcode by overriding the 
'search_query' parameter

## Shortcode parameters
* 'page_title' => The title of the page.
* 'limit' => How many items to fetch.
* 'offset' => 0 for the first item and then zero index into the dataset.
* 'date_format' => 'short-time' for 'd.M.Y HH:MM - HH:MM' or 'begin-end' for full date
* 'image_size' => small, medium, large, xlarge or original
* 'language' => 'is' or 'en'.
* 'display' => 'list' or preview
* 'text_info' => If list, should it display text info or not? 
* 'full_text' => If text info, should it display full text or truncated?
* 'text_length' => The length of the truncated text
* 'base_query' => find?sort=start&format=json
* 'search_query' => &all=Bókasafn Hafnarfjarðar