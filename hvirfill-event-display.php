<?php
/**
 * Plugin Name: Hvirfill Event Display
 * Plugin URI:
 * Description: Fetches events from Hvirfill and displays them with shortcode. Description in README.md
 * Version: 1.0
 * Author: Hilmar Kári Hallbjörnsson - Um að gera ehf.
 * Author URI: https://umadgera.is
 * License: GPLv2
 */
register_activation_hook(__FILE__, 'hvirfill_set_default_options');
add_action('admin_init', 'hvirfill_admin_init');
add_action('admin_menu', 'hvirfill_settings_menu');

/**
 * {@inheritdoc}
 */
function hvirfill_set_default_options() {
    hvirfill_get_options();
}

/**
 * {@inheritdoc}
 */
function hvirfill_get_options() {
    $options = get_option('hvirfill_options', array());

    $new_options['hvirfill_schema'] = 'https';
    $new_options['hvirfill_url'] = 'hvirfill.reykjavik.is';
    $new_options['hvirfill_query_string'] = 'find?sort=start&format=json'; //
    $new_options['hvirfill_search_query'] = '&all=' . urlencode('Bókasafn Hafnarfjarðar');
    $new_options['hvirfill_offset'] = 0;
    $new_options['hvirfill_limit'] = 2;

    $merged_options = wp_parse_args($options, $new_options);

    $compare_options = array_diff_key($new_options, $options);
    if ( empty ($options) || !empty( $compare_options)) {
        update_option('hvirfill_options', $merged_options);
    }

    return $merged_options;
}

/**
 * {@inheritdoc}
 */
function hvirfill_admin_init() {
    // Register a setting group with a validation function
    // so that post data handling is done automatically for us
    register_setting('hvirfill_settings', 'hvirfill_options', 'hvirfill_validate_options');

    // Add a new settings section within the group
    add_settings_section('hvirfill_main_section', 'Main Settings',
        'hvirfill_main_setting_section_callback', 'hvirfill_settings_section');

    // Add each field with its name and function to use for
    // our new settings, put them in our new section
    add_settings_field('hvirfill_schema', 'Schema', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_schema'));

    add_settings_field('hvirfill_url', 'Url', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_url'));

    add_settings_field('hvirfill_query_string', 'Query string', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_query_string'));

    add_settings_field('hvirfill_search_query', 'Search Query', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_search_query'));

    add_settings_field('hvirfill_offset', 'Offset', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_offset'));

    add_settings_field('hvirfill_limit', 'Limit', 'hvirfill_display_text_field',
        'hvirfill_settings_section', 'hvirfill_main_section', array('name' => 'hvirfill_limit'));
}

/**
 * {@inheritdoc}
 */
function hvirfill_validate_options( $input ) {
    foreach( array( 'hvirfill_schema' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }
    foreach( array( 'hvirfill_url' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }
    foreach( array( 'hvirfill_query_string' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }
    foreach( array( 'hvirfill_search_query' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }
    foreach( array( 'hvirfill_offset' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }
    foreach( array( 'hvirfill_limit' ) as $option_name) {
        if( isset( $input[$option_name] ) ) {
            $input[$option_name] = sanitize_text_field( $input[$option_name] );
        }
    }

    return $input;
}

/**
 * {@inheritdoc}
 */
function hvirfill_main_setting_section_callback() { ?>
    <p>This is the main configuration section.</p>

<?php }

/**
 * Displays the data as a text field.
 *
 * @param array $data
 */
function hvirfill_display_text_field( $data = array() ) {
    extract( $data );
    $options = hvirfill_get_options();
    ?>
    <input type="text" name="hvirfill_options[<?php echo $name; ?>]"
           value="<?php echo esc_html( $options[$name] ); ?>"/>
    <br />
<?php }

/**
 * Displays the data as checkbox field.
 * @param array $data
 */
function hvirfill_display_check_box( $data = array() ) {
    extract ( $data );
    $options = hvirfill_get_options();
    ?>
    <input type="checkbox"
           name="hvirfill_options[<?php echo $name;  ?>]"
        <?php checked( $options[$name] ); ?>/>
<?php }

/**
 * {@inheritdoc}
 */
function hvirfill_settings_menu() {
    add_options_page( 'Stillingar fyrir Hvirfil',
        'Hvirfill', 'manage_options',
        'hvirfilsstillingar', 'hvirfill_config_page' );
}

/**
 * {@inheritdoc}
 */
function hvirfill_config_page() { ?>
    <div id="hvirfill-general" class="wrap">
        <h2>Hvirfill - Settings API</h2>

        <form name="hvirfill_options_form_settings_api" method="post"
              action="options.php">

            <?php settings_fields( 'hvirfill_settings' ); ?>
            <?php do_settings_sections( 'hvirfill_settings_section' ); ?>

            <input type="submit" value="Submit" class="button-primary" />
        </form></div>
<?php }

/**
 * {@inheritdoc}
 */
function hvirfill_shortcode( $atts ) {
    $output = "";
    $options = hvirfill_get_options();
    extract( shortcode_atts( array(
        'page_title' => 'Viðburðir',
        'limit' => $options['hvirfill_limit'],
        'offset' => $options['hvirfill_offset'],
        'date_format' => 'short-time',
        'image_size' => 'medium',
        'language' => 'is',
        'display' => 'list',
        'full_text' => FALSE,
        'text_length' => 200,
        'text_info' => FALSE,
        'base_query' => $options['hvirfill_query_string'],
        'search_query' => $options['hvirfill_search_query']
    ), $atts ) );

    $url = hvirfill_generate_url(
        $options['hvirfill_schema'], $options['hvirfill_url'], $base_query,
        $search_query, $limit, $offset
    );

    $data = hvirfill_fetch_data($url);
    $output .= hvirfill_render_list(
        $data, $display, $page_title, $language, $date_format, $image_size, $text_info, $full_text, $text_length
    );

    return $output;
}

/**
 * Adds the URL Schema to the event and formats the date string.
 *
 * @param stdClass $event
 *   The event object, passed by reference
 * @param $language
 *   The language string
 * @param $date_format
 *   The date format to be applied.
 */
function hvirfill_process_event(&$event, $language, $date_format) {
    $options = hvirfill_get_options();
    $event->url_schema = $options['hvirfill_schema']
        . '://' . $options['hvirfill_url'];
    $event->date_time_string = $date_time = hvirfill_parse_date($event->start, $event->end, $date_format, $language);
}

/**
 * Renders a full list view of fetched events
 *
 * @param array $data
 *    Array of data from Hvirfill
 * @param string $display
 *    Defines if we are displaying one item or multiple (single/list)
 * @param string $title
 *    The Title, to display above the data. If empty, the title (and heading) will not be displayed.
 * @param string $language
 *    The language string (en/is)
 * @param string $date_format
 *    Format of the date string (short-time, begin-end)
 * @param string $image_size
 *    How big should the image be (small, medium, large, xlarge, original)
 * @param bool $text_info
 *    Indicates if to display the text info or not (is it a list on a page or preview in a sidebar).
 * @param bool $full_text
 *    Indicates if to display the full text or just part of it.
 * @param int $text_length
 *    Indicates how long the summary is (given that $full_text is FALSE).
 * @return string $output
 *    The output, as a html markup.
 */
function hvirfill_render_list($data, $display, $title, $language, $date_format, $image_size, $text_info, $full_text, $text_length) {
    $output = '<div id="hvirfill-events">';
    if(strlen($title) != 0) {
        $output .= '<h2>' . $title . '</h2>';
    }
    if($display == 'list') {
        $output .= "<div class='event-list'>";
        $output .= "<ul>";
    }
    else {
        $output .= "<div class='event-full-display'>";
    }

    foreach($data as $event) {
        if($display == 'list') {
            $output .= "<li>";
        }

        hvirfill_process_event($event, $language, $date_format);
        $output .= hvirfill_render_event_element($event, $language, $image_size, $text_info, $full_text, $text_length);
        if($display == 'list') {
            $output .= "</li>";
        }
    }
    if($display == 'list') {
        $output .= "</ul>";
    }
    $output .= "</div>"; // event-list or event-full-display
    $output .= '</div>'; // hvirfill-events

    return $output;
}

/**
 * Renders a display of one event.
 *
 * @param $data
 * @param $title
 * @param $language
 * @param $image_size
 * @param bool $text_info
 * @param int $text_length
 * @return string
 */
function hvirfill_render_event_element($event, $language, $image_size, $text_info, $full_text, $text_length) {
    $output = "<div class='event-full'>";
    $output .= "<h3 class='event-header'>" . $event->language->$language->title . "</h3>";
    $output .= "<div class='image-wrapper'>";
    $output .= "<img src='" . $event->url_schema . $event->image->$image_size . "' />";
    $output .= "</div>"; // event-full
    $output .= "<div class='text-wrapper'>";
    $output .= "<div class='time-and-location-wrapper'>";
    $output .= "<span class='date'>" . $event->date_time_string . "</span>";

    $output .= "</div>"; // time-and-location-wrapper
    if($text_info) {
        $text = ($full_text)
            ? $event->language->$language->text
            : (strlen($event->language->$language->text) <= $text_length)
                ? $event->language->$language->text
                : substr($event->language->$language->text, 0, $text_length) . " ...";
        $output .= "<span class='location'>" . $event->formatted_address . "</span>";
        $output .= "<p>" . $text . "</p>";
        $output .= "</div>"; // text-wrapper
        $output .= "<div class='tickets-and-url'>";
        if(isset($event->media->website)) {
            $output .= "<span class='website'><a href='" . $event->media->website . "'>Vefsíða</a></span>";
        }
        if(isset($event->media->facebook)) {
            $output .= "<span class='website'><a href='" . $event->media->facebook . "'>Facebook</a></span>";
        }
        if(isset($event->media->tickets)) {
            $output .= "<span class='website'><a href='" . $event->media->tickets . "'>Miðasala</a></span>";
        }
        $output .= "</div>"; // tickets-and-url
    } else {
        $output .= "</div>"; // text-wrapper (if text info is not displayed)
    }
    $output .= "</div>";

    return $output;
}

/**
 * Parses start and finishing dates in the format YYYY-MM-DD into requested format
 *
 * @param string $start
 *   Starting date/time in the format YYYY-MM-DD HH:MM
 * @param string $finish
 *   Ending date/time in the format YYYY-MM-DD HH:MM
 * @param $format
 *   The output format (short-time, begin-end)
 * @param $language
 *   The language of the time display (is, en)
 * @return string
 *   The preferred string to display
 */
function hvirfill_parse_date($start, $finish, $format, $language) {
    $date = "";
    switch($language) {
        case 'is':
            setlocale(LC_ALL, 'is_IS.UTF8');
            switch ($format) {
                case "short-time":
                    $date = strftime('%d. %B, %Y %H:%M', strtotime($start)) . ' - ' . strftime('%H:%M', strtotime($finish));
                    break;
                case "begin-end":
                    $date = strftime('%d. %B, %Y %H:%M', strtotime($start)) . ' - ' . strftime('%d. %B, %Y %H:%M', strtotime($finish));
                    break;
            }
            break;

        case 'en':
            setlocale(LC_ALL, 'en_GB.UTF8');
            switch ($format) {
                case "short-time":
                    $date = strftime('%B %d, %Y %H:%M', strtotime($start)) . ' - ' . strftime('%H:%M', strtotime($finish));
                    break;
                case "begin-end":
                    $date = strftime('%B. %d, %Y %H:%M', strtotime($start)) . ' - ' . strftime('%B. %d, %Y %H:%M', strtotime($finish));
                    break;
            }
            break;
    }


    return $date;
}

/**
 * Fetches data from a URL.
 *
 * @TODO: This should be re-implemented using better functionality than file_get_contents().
 *
 * @param string $url
 *   The URL to the resource
 * @return string
 *   JSON String with data
 */
function hvirfill_fetch_data($url) {
    $data = json_decode(file_get_contents($url));
    return $data;
}

/**
 * Generates the URL that is passed to the Hvirfill API
 *
 * @param $schema
 *   Is it http or https?
 * @param $url
 *   The URL to call the service (hvirfill.reykjavik.is as of now).
 * @param $query_string
 *   The basic query string
 * @param $search_query
 *   What we are actually searching for.
 * @param $limit
 *   How many items should we fetch
 * @param $offset
 *   From 0 or somewhere inside the dataset
 * @return string
 *   Fully formed url string that the API can respond to
 */
function hvirfill_generate_url($schema, $url, $query_string, $search_query, $limit, $offset) {
    $date = strftime('%Y-%m-%d', time());
    $url = $schema
        . '://' . $url
        . '/' . $query_string . $search_query
        . '&f=' . $date
        . '&offset=' . $offset
        . '&limit=' . $limit;

    return $url;
}

add_shortcode('hvirfill', 'hvirfill_shortcode');